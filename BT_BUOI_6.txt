BT lý thuyết: LÀM THEO HIỂU BIẾT - KHÔNG XEM TÀI LIỆU
1. Ưu điểm của lập trình thủ tục hàm so với lập trình tuyết tính là gì ?
	-Ngắn gọn hơn so với lập trình tuyến tính
	-Có thể tái sử dụng nhiều lần

2. Có mấy loại hàm, kể tên và phân tích loại hàm đó được sử dụng khi nào?
	- Có 2 loại:
		+Hàm trả về: thực hiện tính toán sau đó trả về giá trị để tái sử dụng giá trị đó.
		+Hàm không trả về: thực hiện tính toán nhưng không trả về giá trị mà để in ra màng hình...

3. Chức năng và nguyên lý hoạt động của return trong hàm có kdl trả về
- Dùng để thoát hàm và lấy giá trị trả về cho lời gọi hàm 
- Chỉ trả về 1 giá trị 

4. cách dùng lời gọi hàm của hàm có giá trị trả về ?
< kiểu dữ liệu của hàm > tên_hàm < Tham số truyền vào nếu có >
	{
		//Thân hàm 
	}
tổng, gán, logic, in ra mang hinh

4. tham số hình thức là gì, tham số thực là gì ?
- Tham số thực truyền ở lời gọi hàm 
- Tham số hình thức truyền ở định nghĩa hàm 

5. Tham trị là gì, tham chiếu là gì ?
-Tham trị mọi thay đổi trên bản sao điều không ảnh hưởng đến bản gốc
-Tham chiều mọi thay đổi điều được lưu lại 

6. Hàm trong chương trình được thực thi khi nào ?
- Hàm trong chương trình được thực thi khi gọi hàm con

7. Nếu muốn trả về nhiều hơn 2 giá trị thì ta làm sao ?
Sử 
==============================================================================
------------------------CODE MAU---------------------------------------
/------------ code mẫu ucln, bcnn c1 ----
#include <iostream>
using namespace std;
int ucln(int a,int b)
{
	int min = a;
	if (b < min)
	{
		min = b;
	}
	//-------
	for (int i = min;i > 0;i--)//4321
	{
		if (a%i == 0 && b%i == 0)
		{
			return i;//ucln
		}
	}
}
int bcnn(int a, int b)
{
	int max = a;
	if (b > max)
	{
		max = b;
	}
	//-------

	while (true)//12
	{
		if (max%a == 0 && max%b == 0)
		{
			return max;//12
		}
		max++;//12
	}
}
void main()
{
	
	cout << ucln(4, 6) << endl;
	cout << bcnn(4, 6) << endl;
	system("pause");
}
-===================----------- code mẫu ucln, bcnn c2 ---------
#include <iostream>
using namespace std;
//======================= khu vực khai báo nguyên mẫu hàm =====================
int bcnn(int a, int b);
int ucln(int a, int b);
void min_max(int n);
//================ main ============

void main()
{
	int n;
	cout << "Nhap n: ";cin >> n;
	min_max(n);
	system("pause");
}
//================== khu vực định nghĩa hàm ==============
int bcnn(int a, int b)
{
	return (a*b) / ucln(a, b);
}

int ucln(int a, int b)//4	6
{
	while (a != b)//2	2
	{
		if (a > b)
		{
			a = a - b;//2
		}
		else
		{
			b = b - a;//2
		}
	}
	return a;//b
}
//-----------
void min_max(int n)
{
	//nhap n pt

	int min = INT32_MAX;//min khởi tạo phải lớn hơn những giá trị chúng ta nhập
	int max = INT32_MIN;//nhỏ nhất
	for (int i = 1;i <= n;i++)//i = 1:	x= 99999
	{
		int x;
		cin >> x;
		if (x < min)
		{
			min = x;
		}
		if (x>max)
		{
			max = x;
		}
	}
	cout << "min = " << min << endl;
	cout << "max = " << max << endl;
}
==================code mẫu nạp chồng======================
#include <iostream>
using namespace std;

int tong(int a, int b)
{
	cout << "1" << endl;
	return a + b;
}
int tong(int a, int b, int c)
{
	return a + b + c;
}
float tong(float a, float b)
{
	cout << "2" << endl;
	return a + b;
}
float tong(int a, float b)
{
	cout << "3" << endl;
	return a + b;
}
void main()
{
	int a = 3, b = 7;
	float c = 1.5, d = 3.2;
	cout << tong(a, b,2) << endl;

	system("pause");
}
========================KHUON MAU HAM ==================================================
#include <iostream>
using namespace std;

template <class p1, class p2>//p1: kdl Đại diện thứ 1, p2: kdl đại diện thứ 2
p1 tong(p1 a, p2 b)
{
	return(a + b);
}
void main()
{
	int a = 3, b = 7;
	float c = 1.5, d = 3.2;
	cout << tong(a,b) << endl;//int
	cout << tong(c, b) << endl;//float
	cout << tong(b, c) << endl;//int

	system("pause");
}


============================ BÀI TẬP ỨNG DỤNG // giúp làm quen và ghi nhớ cú pháp===============================

---------- GIẢI BẰNG LẬP TRÌNH THỦ TỤC HÀM --------

1. Viết ct Tính UCLN, BCNN 2 số nguyên a,b
#include <iostream>
using namespace std;

int ucln(int a, int b)
{
    int c;
    while (a % b != 0)
    {
        c = a % b;
        a = b;
        b = c;
    }
    return b;
}

int main()
{
    int a, b, UCLN, BCNN;
    cout << "Nhap vao 2 so nguyen a, b: ";
    cin >> a >> b;
    UCLN = ucln(a, b);
    BCNN = a*b/ ucln(a, b);
    cout << "UCLN : " << UCLN << endl;
    cout << "BCNN : " << BCNN << endl;
    system("pause");
}

2. viết ct nhập n số nguyên, tìm số lớn nhất, nhỏ nhất.// ko sử dụng mảng

#include <iostream>
using namespace std;
int tim_max(int n)
{
	int  i, max, a, b, c;
	cout << "Nhap gia tri thu 1: "; cin >> a;
	max = a;
	for (i = 2; i <= n; i++)
	{
		cout << "Nhap gia tri thu " << i << ": "; cin >> b;
		c = b;
		if (c > max)
		{
			max = c;
		}
	}
	return max;
}
void main()
{
	int n, gtmax ; 
	cout << "Nhap vao n so nguyen duong: "; cin >> n;
	gtmax = tim_max(n); 
	cout << "Gia tri Max la: " << gtmax << endl;
}
-------------------------------------------------------------------------------------------------
3. Viết hàm nhập vào điểm -> xuất ra học lực của học sinh. // 9,10 giỏi, 78 khá, 456 tb, 0123 yếu

#include <iostream>
using namespace std;

void xep_loai(float cc, float gk, float ck)
{
	
	//------------- tinh dtb -------
	float dtb = (cc * 0.1 + gk * 0.3 + ck * 0.6);
	//------------- xep loai -------
	cout << "dtb = " << dtb << endl;
	if (dtb >= 8)
	{
		cout << "Gioi" << endl;
	}
	else if (dtb >= 6.5)
	{
		cout << "Kha" << endl;
	}
	else if (dtb >= 5)
	{
		cout << "Trung Binh" << endl;
	}
	else if (dtb >= 3.5)
	{
		cout << "Yeu" << endl;
	}
	else
	{
		cout << "Kem" << endl;
	}
}
void main()
{
	float cc, gk, ck;
	cout << "Nhap diem chuyen can: ";cin >> cc;
	cout << "Nhap diem giua ky: ";cin >> gk;
	cout << "Nhap diem cuoi ky: ";cin >> ck;
	xep_loai(cc, gk, ck);
}
-------------------------------------------------------------------------------------------------
4. Viết ct hoán vị 2 số nguyên a,b

#include <iostream>
using namespace std;
void hoan_vi(int &a,int &b)
{
	int tam;
	tam = a;
	a = b;
	b = tam;
}
void main()
{
	int a, b;
	cout << "Nhap vao 2 so nguyen a, b: "; cin >> a >> b;
	hoan_vi(a,b);
	cout << "Hoan vi cua a va b la: " << a <<"    " << b << endl;
	system("pause");
}
-------------------------------------------------------------------------------------------------
5. Viết hàm nhập vào tuổi -> xuất ra năm sinh

#include <iostream>
using namespace std;

void nam_sinh (int tuoi)
{
	int nam_sinh;
	nam_sinh = 2021 - tuoi;
	cout << "Nam sinh cua ban la: " << nam_sinh << endl;
}

void main()
{
	int tuoi;
	cout << "Nhap vao tuoi: "; cin >> tuoi;
	nam_sinh(tuoi);

}

-------------------------------------------------------------------------------------------------
6. Số đảo

#include <iostream>
using namespace std;

void so_dao(int n)
{
    int a;
    while (n > 0)
    {
        a = n % 10;
        cout << a;
        n = n / 10;
    }
}

int main()
{
    int n;
    cout << "Nhap vao so nguyen n: ";
    cin >> n;
    so_dao(n);
    system("pause");
}